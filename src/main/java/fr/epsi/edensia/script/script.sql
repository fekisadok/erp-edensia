
DROP TABLE recette;
DROP TABLE recette_ingredient;
DROP TABLE ingredient;

CREATE TABLE recette (
	id_recette SERIAL PRIMARY KEY,
	libelle varchar(100) NOT NULL,
	nbPersonne int4 NULL
	
)
WITH (
	OIDS=FALSE
) ;


CREATE TABLE ingredient (
	id_ingredient SERIAL PRIMARY KEY,
	libelle varchar(100) NOT NULL
)
WITH (
	OIDS=FALSE
) ;

CREATE TABLE public.recette_ingredient (
	id_ingredient_recette SERIAL PRIMARY KEY,
	oid_recette int4 NULL,
	oid_ingredient int4 NULL,
	quantite varchar(100),
	CONSTRAINT recette_ingredient_fk FOREIGN KEY (oid_recette) REFERENCES public.recette(id_recette),
	CONSTRAINT ingredient_recette_fk FOREIGN KEY (oid_ingredient) REFERENCES public.ingredient(id_ingredient)
)
WITH (
	OIDS=FALSE
) ;

insert into public.ingredient
(id_ingredient, libelle)
values (1, 'Oeuf');


insert into public.ingredient
(id_ingredient, libelle)
values (2, 'Chocolat');

insert into public.ingredient
(id_ingredient, libelle)
values (3, 'Levure');

insert into public.ingredient
(id_ingredient, libelle)
values (4, 'Farine');

insert into public.ingredient
(id_ingredient, libelle)
values (5, 'Beurre');