package fr.epsi.edensia.core;

import java.util.Iterator;

import javax.faces.FacesException;
import javax.faces.context.ExceptionHandler;
import javax.faces.context.ExceptionHandlerWrapper;
import javax.faces.event.ExceptionQueuedEvent;
import javax.faces.event.ExceptionQueuedEventContext;

public class RestoreViewExceptionHandler extends ExceptionHandlerWrapper {

	private final ExceptionHandler wrapped;

	RestoreViewExceptionHandler(final ExceptionHandler exception) {
		this.wrapped = exception;
	}

	@Override
	public ExceptionHandler getWrapped() {
		return wrapped;
	}

	@Override
	public void handle() throws FacesException {

		final Iterator<ExceptionQueuedEvent> i = getUnhandledExceptionQueuedEvents()
				.iterator();
		while (i.hasNext()) {
			ExceptionQueuedEvent event = i.next();
			ExceptionQueuedEventContext context = (ExceptionQueuedEventContext) event
					.getSource();

			Throwable t = context.getException();

			t.fillInStackTrace().getClass().getSimpleName();
			// Gestion du bug de restore View

			if (t != null && t.getCause() != null && t.getClass() != null) {
				if (("java.lang.IndexOutOfBoundsException: Index: 0, Size: 0")
						.equals(t.getCause().toString())
						&& t.getClass().equals(FacesException.class)) {

					i.remove();
				}
			}

		}
		// parent hanle
		getWrapped().handle();
	}

}
