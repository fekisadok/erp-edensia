package fr.epsi.edensia.core;

import java.util.Map;

import javax.faces.context.FacesContext;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.config.Scope;

/**
 * Implements the JSF View Scope for use by Spring. This class is registered as
 * a Spring bean with the CustomScopeConfigurer.
 */
public class ViewScope implements Scope {

	@Override
	public Object get(final String name, final ObjectFactory<?> objectFactory) {

		if (FacesContext.getCurrentInstance().getViewRoot() != null) {
			final Map<String, Object> viewMap = FacesContext
					.getCurrentInstance().getViewRoot().getViewMap();
			if (viewMap.containsKey(name)) {
				return viewMap.get(name);
			} else {
				final Object object = objectFactory.getObject();
				viewMap.put(name, object);
				return object;
			}
		} else {
			return null;
		}
	}

	@Override
	public Object remove(final String name) {
		if (FacesContext.getCurrentInstance().getViewRoot() != null) {
			return FacesContext.getCurrentInstance().getViewRoot().getViewMap()
					.remove(name);
		} else {
			return null;
		}
	}

	@Override
	public void registerDestructionCallback(final String name,
			final Runnable callback) {
		// Do nothing
	}

	@Override
	public Object resolveContextualObject(final String key) {
		return null;
	}

	@Override
	public String getConversationId() {
		return null;
	}

}
