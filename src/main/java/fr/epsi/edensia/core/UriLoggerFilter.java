package fr.epsi.edensia.core;

import javax.faces.context.FacesContext;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;

import org.apache.log4j.Logger;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

public class UriLoggerFilter implements PhaseListener {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = Logger
			.getLogger(UriLoggerFilter.class);

	/**
     * 
     */
	private static final long serialVersionUID = 1L;

	@Override
	public void afterPhase(final PhaseEvent event) {
	}

	@Override
	public void beforePhase(final PhaseEvent event) {

		if (LOGGER.isInfoEnabled()) {

			final Authentication authentication = SecurityContextHolder
					.getContext().getAuthentication();

			final FacesContext fc = event.getFacesContext();

			if (fc != null && fc.getViewRoot() != null) {

				if (authentication != null) {

					if (fc.getExternalContext().getRequestParameterMap()
							.get("javax.faces.source") != null
							&& fc.getExternalContext().getRequestParameterMap()
									.get("javax.faces.source")
									.contains("form_notification")) {

						if (LOGGER.isDebugEnabled()) {
							LOGGER.debug("Acces uri (pool notification) : "
									+ fc.getViewRoot().getViewId() + " User : "
									+ authentication.getName());
						}

					} else if (fc.getExternalContext().getRequestParameterMap()
							.get("refresh") == null
							|| !fc.getExternalContext()
									.getRequestParameterMap().get("refresh")
									.equals("true")) {
						if (LOGGER.isInfoEnabled()) {
							LOGGER.info("Acces uri : "
									+ fc.getViewRoot().getViewId() + " User : "
									+ authentication.getName());
						}
					}
				} else {
					if (LOGGER.isInfoEnabled()) {
						LOGGER.info("Acces uri : "
								+ fc.getViewRoot().getViewId()
								+ " User : No authenticate ");
					}
				}
			}

		}

	}

	@Override
	public PhaseId getPhaseId() {
		return PhaseId.RENDER_RESPONSE;
	}

}
