package fr.epsi.edensia.facade.api;

import java.util.List;

import fr.epsi.edensia.domaine.IngredientPOJO;
import fr.epsi.edensia.domaine.IngredientRecettePOJO;
import fr.epsi.edensia.domaine.RecettePOJO;

public interface ICommunFacade {

	List<RecettePOJO> findAllRecette();

	RecettePOJO getRecette(Integer idRecette);

	void saveOrUpdateRecette(RecettePOJO recette);

	void removeRecette(RecettePOJO recette);

	List<IngredientPOJO> findAllIngredient();

	IngredientPOJO findIngredient(Integer idIngredient);

	void saveOrUpdateIngredient(IngredientPOJO ingredient);

	void removeIngredient(IngredientPOJO ingredient);

	List<IngredientRecettePOJO> findAllIngredientRecette();

	IngredientRecettePOJO getIngredientRecette(Integer idIngredientRecette);

	void saveOrUpdateIngredientRecette(IngredientRecettePOJO ingredientRecette);

	void removeIngredientRecette(IngredientRecettePOJO ingredientRecette);

	List<IngredientRecettePOJO> findIngredientByRecette(Integer idRecette);

}
