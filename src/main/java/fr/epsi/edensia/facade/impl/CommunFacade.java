package fr.epsi.edensia.facade.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.epsi.edensia.domaine.IngredientPOJO;
import fr.epsi.edensia.domaine.IngredientRecettePOJO;
import fr.epsi.edensia.domaine.RecettePOJO;
import fr.epsi.edensia.facade.api.ICommunFacade;
import fr.epsi.edensia.service.api.IIngredientRecetteService;
import fr.epsi.edensia.service.api.IIngredientService;
import fr.epsi.edensia.service.api.IRecetteService;

@Service("communFacade")
public class CommunFacade implements ICommunFacade {

	// INJECTION
	@Autowired
	private IRecetteService recetteService;

	@Autowired
	private IIngredientService ingredientService;

	@Autowired
	private IIngredientRecetteService ingredientRecetteService;
	// INJECTION

	@Override
	public List<RecettePOJO> findAllRecette() {
		return this.recetteService.findAllRecette();
	}

	@Override
	public RecettePOJO getRecette(Integer idRecette) {
		return this.recetteService.getRecette(idRecette);
	}

	@Override
	public void saveOrUpdateRecette(RecettePOJO recette) {
		this.recetteService.saveRecette(recette);
	}

	@Override
	public void removeRecette(RecettePOJO recette) {
		this.recetteService.removeRecette(recette);
	}

	@Override
	public List<IngredientPOJO> findAllIngredient() {
		return this.ingredientService.findAllIngredient();
	}

	@Override
	public IngredientPOJO findIngredient(Integer idIngredient) {
		return this.ingredientService.getIngredient(idIngredient);
	}

	@Override
	public void saveOrUpdateIngredient(IngredientPOJO ingredient) {
		this.ingredientService.saveIngredient(ingredient);
	}

	@Override
	public void removeIngredient(IngredientPOJO ingredient) {
		this.ingredientService.removeIngredient(ingredient);
	}

	@Override
	public List<IngredientRecettePOJO> findAllIngredientRecette() {
		return this.ingredientRecetteService.findAllIngredientRecette();
	}

	@Override
	public IngredientRecettePOJO getIngredientRecette(Integer idIngredientRecette) {
		return this.ingredientRecetteService.getIngredientRecette(idIngredientRecette);
	}

	@Override
	public void saveOrUpdateIngredientRecette(IngredientRecettePOJO ingredientRecette) {
		this.ingredientRecetteService.saveIngredientRecette(ingredientRecette);
	}

	@Override
	public void removeIngredientRecette(IngredientRecettePOJO ingredientRecette) {
		this.ingredientRecetteService.removeIngredientRecette(ingredientRecette);
	}

	@Override
	public List<IngredientRecettePOJO> findIngredientByRecette(Integer idRecette) {
		return this.ingredientRecetteService.findIngredientByRecette(idRecette);
	}
}
