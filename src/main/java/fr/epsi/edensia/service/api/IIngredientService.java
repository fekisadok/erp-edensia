package fr.epsi.edensia.service.api;

import java.util.List;

import fr.epsi.edensia.domaine.IngredientPOJO;

public interface IIngredientService {

	List<IngredientPOJO> findAllIngredient();

	IngredientPOJO getIngredient(Integer idIngredient);

	void saveIngredient(IngredientPOJO ingredient);

	void removeIngredient(IngredientPOJO ingredient);

}
