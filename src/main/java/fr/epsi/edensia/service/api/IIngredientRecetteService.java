package fr.epsi.edensia.service.api;

import java.util.List;

import fr.epsi.edensia.domaine.IngredientRecettePOJO;

public interface IIngredientRecetteService {

	List<IngredientRecettePOJO> findAllIngredientRecette();

	IngredientRecettePOJO getIngredientRecette(Integer idIngredientRecette);

	void saveIngredientRecette(IngredientRecettePOJO ingredientRecette);

	void removeIngredientRecette(IngredientRecettePOJO ingredientRecette);

	List<IngredientRecettePOJO> findIngredientByRecette(Integer idRecette);

}
