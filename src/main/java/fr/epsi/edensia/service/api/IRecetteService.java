package fr.epsi.edensia.service.api;

import java.util.List;

import fr.epsi.edensia.domaine.RecettePOJO;

public interface IRecetteService {

	void removeRecette(RecettePOJO recette);

	void saveRecette(RecettePOJO recette);

	RecettePOJO getRecette(Integer idRecette);

	List<RecettePOJO> findAllRecette();

}
