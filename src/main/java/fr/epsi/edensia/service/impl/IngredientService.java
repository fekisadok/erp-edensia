package fr.epsi.edensia.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.epsi.edensia.dao.api.IIngredientDAO;
import fr.epsi.edensia.domaine.IngredientPOJO;
import fr.epsi.edensia.domaine.RecettePOJO;
import fr.epsi.edensia.service.api.IIngredientService;

@Service("ingredientService")
public class IngredientService implements IIngredientService {

	//INJECTION
	@Autowired
	private IIngredientDAO ingredientDAO;
	//INJECTION
	
	@Override
	public List<IngredientPOJO> findAllIngredient(){
		return this.ingredientDAO.findAll();
	}
	
	@Override
	public IngredientPOJO getIngredient(Integer idIngredient) {
		return this.ingredientDAO.get(idIngredient);
	}
	
	@Override
	public void saveIngredient(IngredientPOJO ingredient) {
		this.ingredientDAO.save(ingredient);
	}
	
	@Override
	public void removeIngredient(IngredientPOJO ingredient) {
		this.ingredientDAO.delete(ingredient);
	}
}
