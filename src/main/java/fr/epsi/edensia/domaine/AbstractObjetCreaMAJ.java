package fr.epsi.edensia.domaine;

import java.io.Serializable;

import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

@MappedSuperclass
public abstract class AbstractObjetCreaMAJ implements Serializable,
		IObjetCreaMAJ {

	/**
    * 
    */
	private static final long serialVersionUID = -7817137025734545279L;

	private Integer id;

	/**
	 * @return the id Integer
	 */
	@Transient
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 *            Integer
	 */
	public void setId(final Integer id) {
		this.id = id;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		// CHECKSTYLE:OFF
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		// CHECKSTYLE:ON
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		// CHECKSTYLE:OFF
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final AbstractObjetCreaMAJ other = (AbstractObjetCreaMAJ) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		// CHECKSTYLE:ON
		return true;
	}

	@Transient
	public String toActionEventMessageGlobal(final String message,
			final String action) {

		if ("saveOrUpdate".equals(action) || "save".equals(action)) {
			return "Vous avez enregistr� " + message;
		} else if ("delete".equals(action)) {
			return "Vous avez supprim� " + message;
		}
		return "";

	}

	@Transient
	@Override
	public String getIdItem() {

		return String.valueOf(getId());
	}

}
