package fr.epsi.edensia.dto;

import java.util.ArrayList;
import java.util.List;

public class FeedBackExceptionDTO {

	/** The Constant serialVersionUID. */
	@SuppressWarnings("unused")
	private static final long serialVersionUID = -2249918789304893301L;

	// private Set<String> listError = new HashSet<String>();
	// private Set<String> listWarningWithConfirm = new HashSet<String>();
	// private Set<String> listInformation = new HashSet<String>();

	// la key est le message, la valeur associ� est le type de message �
	// afficher
	/** The list messages. */
	private List<String> listMessages = new ArrayList<String>();

	/**
	 * Constructeur par defaut.
	 */
	public FeedBackExceptionDTO() {
		super();
	}

	/**
	 * Constructeur complet.
	 * 
	 * @param listMessages
	 *            la liste des erreurs
	 */
	public FeedBackExceptionDTO(final List<String> listMessages) {
		super();
		this.listMessages = listMessages;
	}

	/**
	 * Constructeur complet.
	 * 
	 * @param libelleMessage
	 *            String
	 * @param typeAlerte
	 *            String
	 */
	public FeedBackExceptionDTO(final String libelleMessage) {
		this();
		addMessage(libelleMessage);
	}

	/**
	 * Ajoute une erreur a la liste.
	 * 
	 * @param libelleMessage
	 *            String
	 * @param typeAlerte
	 *            String
	 */
	public final void addMessage(final String libelleMessage) {
		if (listMessages == null) {
			listMessages = new ArrayList<String>();
		}
		listMessages.add(libelleMessage);
	}

	/**
	 * Gets the list messages.
	 * 
	 * @return the listMessages
	 */
	public List<String> getListMessages() {
		return listMessages;
	}

	/**
	 * Sets the list messages.
	 * 
	 * @param listMessages
	 *            the listMessages to set
	 */
	public void setListMessages(final List<String> listMessages) {
		this.listMessages = listMessages;
	}

	/**
	 * Checks for error.
	 * 
	 * @return true, if successful
	 */
	public boolean hasError() {

		return !(listMessages == null || listMessages.isEmpty());
	}

	/**
	 * @return String
	 */
	@Override
	public String toString() {
		final StringBuilder toReturn = new StringBuilder();
		contructToString(toReturn);
		return toReturn.toString();
	}

	/**
	 * @param toReturn
	 *            StringBuilder
	 * @param severity
	 *            EnumFeedBackSeverity
	 */
	private void contructToString(final StringBuilder toReturn) {
		final List<String> list = listMessages;
		if (list == null || list.isEmpty()) {
			for (final String message : list) {
				if (!toReturn.toString().isEmpty()) {
					toReturn.append(", ");
				}
				toReturn.append(message);
			}
		}
	}
}
