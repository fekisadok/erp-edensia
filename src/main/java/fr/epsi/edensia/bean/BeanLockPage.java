package fr.epsi.edensia.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import fr.epsi.edensia.domaine.AbstractObjetCreaMAJ;

@Component("beanLockPage")
@Scope("application")
@Lazy(true)
public class BeanLockPage implements Serializable {

	// ATTENTION BEAN APPLICATION : PARTAGER PAR TOUT LE MONDE !!!!!

	// DEBUT INJECTION SPRING.

	/**
     * 
     */
	private static final long serialVersionUID = 1L;
	
	@SuppressWarnings("unused")
	/** The Constant LOGGER. */
	private static final Logger LOGGER = Logger.getLogger(BeanLockPage.class);

	private final List<LockPageDTO> listeAccesLock = new ArrayList<LockPageDTO>();

	/**
	 * PostConstruct.
	 */
	@PostConstruct
	public void init() {

	}

	public boolean isLock(final String sessionId, final String userCode,
			final String viewId, final Object object) {

		if (this.listeAccesLock != null) {

			for (LockPageDTO currentLock : this.listeAccesLock) {

				if (currentLock.getViewId().equals(viewId)
						&& currentLock.getObject().equals(object)
						&& !currentLock.getUserCode().equals(userCode)) {

					if (!(object instanceof AbstractObjetCreaMAJ)
							|| (object instanceof AbstractObjetCreaMAJ && ((AbstractObjetCreaMAJ) object)
									.getId() != null)) {

						return true;
					}

				}
			}
		}

		return false;

	}

	public void addLock(final String sessionId, final String userCode,
			final String viewId, final Object object) {

		LockPageDTO lock = new LockPageDTO(sessionId, userCode, viewId, object);

		if (!listeAccesLock.contains(lock)) {
			listeAccesLock.add(lock);
		}
	}

	public void removeLock(final String sessionId, final String userCode,
			final String viewId, final Object object) {

		this.listeAccesLock.remove(new LockPageDTO(sessionId, userCode, viewId,
				object));

	}

	public void removeLockSessionDestroy(final String sessionId) {

		if (this.listeAccesLock != null) {

			Iterator<LockPageDTO> it = this.listeAccesLock.iterator();
			while (it.hasNext()) {

				LockPageDTO currentLock = it.next();

				if (currentLock.getSessionId().equals(sessionId)) {
					it.remove();
				}
			}

		}
	}

	public void forceRemoveLock() {

		if (this.listeAccesLock != null) {

			Iterator<LockPageDTO> it = this.listeAccesLock.iterator();

			while (it.hasNext()) {

				LockPageDTO currentLock = it.next();

				if (currentLock.getViewId().equals(
						FacesContext.getCurrentInstance().getViewRoot()
								.getViewId())) {
					it.remove();
				}

			}

		}
	}

	public String getCodeUserLock() {

		if (this.listeAccesLock != null) {

			for (LockPageDTO currentLock : this.listeAccesLock) {

				if (currentLock.getViewId().equals(
						FacesContext.getCurrentInstance().getViewRoot()
								.getViewId())) {
					return currentLock.getUserCode();
				}
			}

		}

		return "";

	}

	public class LockPageDTO {

		private String sessionId;
		private String userCode;
		private String viewId;
		private Object object;

		public LockPageDTO(final String nSessionId, final String nUserCode,
				final String nViewId, final Object nObject) {

			this.sessionId = nSessionId;
			this.userCode = nUserCode;
			this.viewId = nViewId;
			this.object = nObject;
		}

		/**
		 * @return the sessionId
		 */
		public String getSessionId() {
			return sessionId;
		}

		/**
		 * @param sessionId
		 *            the sessionId to set
		 */
		public void setSessionId(final String sessionId) {
			this.sessionId = sessionId;
		}

		/**
		 * @return the userCode
		 */
		public String getUserCode() {
			return userCode;
		}

		/**
		 * @param userCode
		 *            the userCode to set
		 */
		public void setUserCode(final String userCode) {
			this.userCode = userCode;
		}

		/**
		 * @return the viewId
		 */
		public String getViewId() {
			return viewId;
		}

		/**
		 * @param viewId
		 *            the viewId to set
		 */
		public void setViewId(final String viewId) {
			this.viewId = viewId;
		}

		/**
		 * @return the object
		 */
		public Object getObject() {
			return object;
		}

		/**
		 * @param object
		 *            the object to set
		 */
		public void setObject(final Object object) {
			this.object = object;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Object#hashCode()
		 */
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + getOuterType().hashCode();
			result = prime * result
					+ ((object == null) ? 0 : object.hashCode());
			result = prime * result
					+ ((sessionId == null) ? 0 : sessionId.hashCode());
			result = prime * result
					+ ((userCode == null) ? 0 : userCode.hashCode());
			result = prime * result
					+ ((viewId == null) ? 0 : viewId.hashCode());
			return result;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Object#equals(java.lang.Object)
		 */
		@Override
		public boolean equals(final Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			LockPageDTO other = (LockPageDTO) obj;
			if (!getOuterType().equals(other.getOuterType()))
				return false;
			if (object == null) {
				if (other.object != null)
					return false;
			} else if (!object.equals(other.object))
				return false;
			if (sessionId == null) {
				if (other.sessionId != null)
					return false;
			} else if (!sessionId.equals(other.sessionId))
				return false;
			if (userCode == null) {
				if (other.userCode != null)
					return false;
			} else if (!userCode.equals(other.userCode))
				return false;
			if (viewId == null) {
				if (other.viewId != null)
					return false;
			} else if (!viewId.equals(other.viewId))
				return false;
			return true;
		}

		private BeanLockPage getOuterType() {
			return BeanLockPage.this;
		}

	}

}
