
package fr.epsi.edensia.bean;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.primefaces.PrimeFaces;

import fr.epsi.edensia.exception.ValidationException;
import fr.epsi.edensia.exception.ValidationWarning;

@Aspect
public class AspectsBean {

	private static final Logger LOGGER = Logger.getLogger(AspectsBean.class);

	@SuppressWarnings("deprecation")
	@Around(value = "execution(* fr.epsi.edensia.bean.*.*.*(..))")
	public Object around(final ProceedingJoinPoint pjp) throws Throwable {

		try {
			return pjp.proceed();

		} catch (ValidationWarning vw) {

			PrimeFaces.current().ajax().addCallbackParam("validationFailed", true);

			// ici faire FACES MESSAGE WARNING

			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("Dans catch ValidationWarning aspect bean : execution(* fr.epsi.edensia.bean.*.*.*(..))",
						vw);
			}
			final List<String> errors = vw.getFeedBack().getListMessages();
			if (!errors.isEmpty()) {

				for (final String current : errors) {
					FacesContext.getCurrentInstance().addMessage(null,
							new FacesMessage(FacesMessage.SEVERITY_WARN, current, ""));
				}
			}
		} catch (ValidationException ve) {
			// ici faire FACES MESSAGE EXCEPTION

			PrimeFaces.current().ajax().addCallbackParam("validationFailed", true);

			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("Dans catch ValidationException aspect bean : execution(* fr.epsi.edensia.bean.*.*.*(..))",
						ve);
			}
			final List<String> errors = ve.getFeedBack().getListMessages();
			if (!errors.isEmpty()) {

				for (final String current : errors) {
					FacesContext.getCurrentInstance().addMessage(null,
							new FacesMessage(FacesMessage.SEVERITY_ERROR, current, ""));
				}
			}
		}

		return null;

	}

}
