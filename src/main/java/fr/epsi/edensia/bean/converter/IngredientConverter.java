
package fr.epsi.edensia.bean.converter;

import java.util.List;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import org.apache.commons.lang3.StringUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.web.jsf.FacesContextUtils;

import fr.epsi.edensia.domaine.IngredientPOJO;
import fr.epsi.edensia.facade.api.ICommunFacade;

@FacesConverter("ingredientConverter")
public class IngredientConverter implements Converter {

	@Override
	public Object getAsObject(final FacesContext fc, final UIComponent uic, final String value) {

		if (value != null && StringUtils.isNumeric(value)) {

			@SuppressWarnings("unchecked")
			final List<IngredientPOJO> liste = (List<IngredientPOJO>) uic.getAttributes().get("listeIngredients");

			// la liste peut etre null si il refont pas l'autocomplete donc
			// findJoueur plus bas !
			if (liste != null) {
				for (final IngredientPOJO current : liste) {
					if (current.getId().equals(Integer.valueOf(value))) {
						return current;
					}
				}
			}

			// ici faire plutot un find en base sur id ! pour gerer le cas du
			// lien sur une localite archive qui remonte pas dans listeLocalites
			// !

			final ApplicationContext ctx = FacesContextUtils
					.getWebApplicationContext(FacesContext.getCurrentInstance());
			final ICommunFacade communFacade = (ICommunFacade) ctx.getBean("communFacade");

			return communFacade.findIngredient(Integer.valueOf(value));
		}

		return null;
	}

	@Override
	public String getAsString(final FacesContext fc, final UIComponent uic, final Object object) {
		if (object != null && object instanceof IngredientPOJO) {
			return ((IngredientPOJO) object).getIdItem();
		} else {
			return null;
		}
	}
}
