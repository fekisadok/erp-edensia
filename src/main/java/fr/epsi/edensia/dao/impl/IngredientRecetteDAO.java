package fr.epsi.edensia.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import fr.epsi.edensia.dao.api.IIngredientRecetteDAO;
import fr.epsi.edensia.domaine.IngredientRecettePOJO;

@Repository("ingredientRecetteDAO")
public class IngredientRecetteDAO extends GenericHibernateDaoImpl<IngredientRecettePOJO, Integer>
		implements IIngredientRecetteDAO {

	@SuppressWarnings("unchecked")
	@Override
	public List<IngredientRecettePOJO> findlisteIngredientsByRecette(final Integer idRecette) {
		final Map<String, Object> mapParameters = new HashMap<String, Object>();

		final StringBuilder hqlQuery = new StringBuilder(" from IngredientRecettePOJO as ingredient ");
		hqlQuery.append(" left join fetch ingredient.recette as recette ");
		hqlQuery.append(" where 1 = 1 ");

		if (idRecette != null) {
			mapParameters.put("idRecette", idRecette);
			hqlQuery.append(" and recette.id = :idRecette ");
		}

		final Query query = em.createQuery(hqlQuery.toString());

		if (mapParameters != null && !mapParameters.isEmpty()) {

			for (final Map.Entry<String, Object> mapEntry : mapParameters.entrySet()) {
				query.setParameter(mapEntry.getKey(), mapEntry.getValue());
			}
		}
		return query.getResultList();
	}

}
