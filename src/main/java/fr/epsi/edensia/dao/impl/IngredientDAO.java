package fr.epsi.edensia.dao.impl;

import org.springframework.stereotype.Repository;

import fr.epsi.edensia.dao.api.IIngredientDAO;
import fr.epsi.edensia.domaine.IngredientPOJO;

@Repository("ingredientDAO")
public class IngredientDAO extends GenericHibernateDaoImpl<IngredientPOJO, Integer> implements IIngredientDAO {

}
