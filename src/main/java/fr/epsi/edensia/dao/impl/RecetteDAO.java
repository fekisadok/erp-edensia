package fr.epsi.edensia.dao.impl;

import org.springframework.stereotype.Repository;

import fr.epsi.edensia.dao.api.IRecetteDAO;
import fr.epsi.edensia.domaine.RecettePOJO;

@Repository("recetteDAO")
public class RecetteDAO extends GenericHibernateDaoImpl<RecettePOJO, Integer> implements IRecetteDAO {

}
