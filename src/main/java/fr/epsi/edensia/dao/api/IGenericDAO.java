package fr.epsi.edensia.dao.api;

import java.io.Serializable;
import java.util.List;

/**
 * Generic DAO class.
 * 
 * @author lincolns, wallacew
 * @param <T>
 * @param <PK>
 */
public interface IGenericDAO<T, PK extends Serializable> {

	/**
	 * Persist the newInstance object into database.
	 * 
	 * @param newInstance to save
	 * @return The identifier
	 **/
	PK save(T newInstance);

	/**
	 * Save or update.
	 * 
	 * @param transientObject to save
	 */
	void saveOrUpdate(T transientObject);

	/**
	 * Retrieve a persisted object with a given id from the database.
	 * 
	 * @param id to load
	 * @return An object of type T
	 */
	// T load(PK id);

	/**
	 * Retrieve a persisted object with a given id from the database.
	 * 
	 * @param id to get
	 * @return An object of type T
	 */
	T get(PK id);

	/**
	 * @param id
	 * @param nullEmptyResult
	 * @return
	 */
	T get(PK id, boolean returnNullIfEmptyResult);

	// /**
	// * @param id
	// * @return
	// */
	// T getEager(PK id);

	/**
	 * Save changes made to a persistent object.
	 * 
	 * @param transientObject object to update
	 **/
	void update(T transientObject);

	/**
	 * Remove the given object from persistent storage in the database.
	 * 
	 * @param persistentObject object to delete.
	 **/
	void delete(T persistentObject);

	/**
	 * Refreshes the object of type T.
	 * 
	 * @param persistentObject to refresh
	 */
	void refresh(T persistentObject);

	/**
	 * Returns a list of objects.
	 * 
	 * @return list of objects
	 */
	List<T> findAll();

	/**
	 * @param ordres
	 * @return
	 */
	List<T> findAll(List<String[]> ordres);

	/**
	 * Flushes the cache of the currently-used session.
	 */
	void flush();

	/**
	 * @return Class<T> getPersistentClass
	 */
	Class<T> getPersistentClass();

	/**
	 * Initializz object.
	 * 
	 * @param list List<T>
	 */
	void initialize(List<T> list);

	/**
	 * @param entity T
	 * @return T
	 */
	T merge(T entity);

	/**
	 * 
	 */
	void commitTransaction();

	/**
	 * 
	 * @param id
	 * @return
	 */
	T getEager(PK id);

}
