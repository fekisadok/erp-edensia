
package test.facade;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.NoResultException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import fr.epsi.edensia.domaine.RecettePOJO;
import fr.epsi.edensia.facade.api.ICommunFacade;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/applicationContext-test.xml" })
public class TestCommunFacade {

	@Autowired
	private ICommunFacade communFacade;

	@Test
	@Rollback(true)
	@Transactional(propagation = Propagation.REQUIRED)
	public void testfindAllRecettes() {
		List<RecettePOJO> listeRecettes = this.communFacade.findAllRecette();

		if (listeRecettes == null || listeRecettes.isEmpty()) {
			fail("ne peut pas etre null");
		}

		assertTrue(true);

	}

	@Test
	@Rollback(true)
	@Transactional(propagation = Propagation.REQUIRED)
	public void testGetRecette() {
		RecettePOJO recette = this.communFacade.getRecette(1);

		if (recette == null) {
			fail("ne peut pas etre null");
		}

		try {
			recette = this.communFacade.getRecette(9999);
			fail("doit etre null");
		} catch (NoResultException nre) {
			assertTrue(true);
		}

		assertTrue(true);

	}

}